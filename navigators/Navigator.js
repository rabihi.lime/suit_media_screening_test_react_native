import React from 'react';
import { NavigationContainer, useNavigation, useNavigationContainerRef } from '@react-navigation/native';
import { createStackNavigator, } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import Main from '../screens/Main';
import Home from '../screens/Home';
import User from '../screens/User';
import WebSite from '../screens/WebSite';
import { Icon, Button } from 'native-base';

const Stack = createStackNavigator();

export default function Navigator() {

    const navigationRef = useNavigationContainerRef();
    const config = {
        screens: {
            MultiPlay: 'play/:passcode'
        },
    };
    const linking = {
        prefixes: ['https://*.kuis.us', 'https://kuis.us'],
        config
    };
    return <SafeAreaProvider>
        <NavigationContainer linking={linking} ref={navigationRef}>
            <Stack.Navigator screenOptions={{ headerShown: false, headerBackTitleVisible: false }}>
                <Stack.Screen name="Main" component={Main} options={{
                    headerShown: false,
                    title: "",
                    headerTitle: "",
                    headerLeft: () => 
                        <Icon
                          onPress={() => alert('This is a button!')}
                          name="close"
                          color="#00cc00"
                        />
                      

                }} />
                <Stack.Screen name="Home" component={Home} options={{
                    headerShown: true,
                    headerTitle: "Home",
                    headerTitleAlign : 'center',
                    
                }} />
                <Stack.Screen name="User" component={User} options={{
                    headerShown: true,
                    headerTitle: "User",
                    headerTitleAlign : 'center',
                    
                }} />
                <Stack.Screen name="WebSite" component={WebSite} options={{
                    headerShown: true,
                    
                }} />
            </Stack.Navigator>
        </NavigationContainer>
    </SafeAreaProvider>
}