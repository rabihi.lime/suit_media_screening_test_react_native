import React, {Component} from 'react';
import { Dimensions, StyleSheet, BackHandler, Platform, View, 
    Text, Image, TouchableOpacity, Alert, ToastAndroid, ScrollView, ImageBackground} from "react-native";
import {
    Button,
    Container,
    Card,
    CardItem,
    Body,
    Content,
    Header,
    Title,
    Left,
    Icon,
    Right, Item, Label, Input, Row, Col, Form
  } from "native-base";
import { StatusBar } from 'expo-status-bar';
import { render } from 'react-dom';
import { KeyboardAwareScrollView, KeyboardAwareFlatList } from 'react-native-keyboard-aware-scroll-view';
import { setSingleUsername } from '../actions/User';
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { WebView } from 'react-native-webview';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

class WebSite extends Component{
    
    constructor(props){
        super(props);

        this.state = {
            name : "",
            palindrome : ""
        }

    }

  
    render(){

        return(
            <Container>

                <View style={styles.content}>
                    <WebView source={{ uri: 'https://suitmedia.com/' }} />
                </View>
                
            </Container>
        )

    }
}

const styles = StyleSheet.create({

    container: {
        flex: 1,
    },
    image: {
        flex: 1,
        justifyContent: "center"
    },
    content: {
        flexGrow: 1,
        padding: 14,
        width: "100%",
      },
    headerImage: {
        alignItems : 'center',
        justifyContent : 'center',
        height : 182,
        backgroundColor : 'transparent',
        marginTop:windowHeight * 10/100
    },
    input: {
        fontSize: 14, 
        backgroundColor:'#FFF', 
        borderRadius:10,
        marginBottom : 13,
        height: 40
    },
    buttonContainer: {
        width: "100%",
        justifyContent: "center",
        alignItems: "center",
        alignSelf : 'center',
        marginTop: 35,
    },
    button: {
        backgroundColor : '#2B637B',
        width : '100%',
        justifyContent :'center',
        height:40,
        borderRadius: 10,
        marginBottom : 13,
    },
})

WebSite.propTypes = {
    errors: PropTypes.object.isRequired
  };
  
const mapStateToProps = state => ({
errors: state.errors,
user : state.users
});
  
export default connect(
    mapStateToProps,
    { setSingleUsername }
  )(WebSite);