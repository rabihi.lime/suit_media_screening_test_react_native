import React, {Component} from 'react';
import { Dimensions, StyleSheet, BackHandler, Platform, View, 
    Text, Image, TouchableOpacity, Alert, ToastAndroid, ScrollView, ImageBackground} from "react-native";
import {
    Button,
    Container,
    Card,
    CardItem,
    Body,
    Content,
    Header,
    Title,
    Left,
    Icon,
    Right, Item, Label, Input, Row, Col, Form, List, ListItem
  } from "native-base";
import { StatusBar } from 'expo-status-bar';
import { render } from 'react-dom';
import { KeyboardAwareScrollView, KeyboardAwareFlatList } from 'react-native-keyboard-aware-scroll-view';
import { setSingleUsername, getUserProfile, setSingleProfile } from '../actions/User';
import { connect } from "react-redux";
import PropTypes from "prop-types";
import MapView, {Marker, PROVIDER_GOOGLE, Polyline} from 'react-native-maps';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

class User extends Component{
    
    constructor(props){
        super(props);

        this.state = {
            name : "",
            palindrome : "",
            maps : false,
            region : {
                latitude: -6.92159288169113,
                longitude: 107.610,
                latitudeDelta: 0.20,
                longitudeDelta: 0.50,
            },
        }
        this.props.navigation.setOptions({

            headerRight: () => 
            <Icon
            onPress={() => this.maps()}
            name="arrow-back"
            color="#2B637B"
            />
        })
    }
    

    componentDidMount(){
        this.props.getUserProfile()
    }

    

    maps=()=>{
        this.setState({maps:!this.state.maps})
    }


    selectProfile(profile){
        this.props.setSingleProfile(profile)
        this.props.navigation.navigate('Home')
    }

    renderListContent (){
        return this.props.user.user_profile.map((profile, index) => {
          const { name } = profile //destructuring
          return (
              <ListItem key={index} >
                <TouchableOpacity onPress = {() => {this.selectProfile(profile)}}>
                  <View style={{flexDirection:'row'}}>  
                      <View style={{flexGrow:1}}>
                        <Image source={{uri: profile.avatar}} style={styles.image_profile} />
                      </View>  
                      <View style={{flexGrow:1, marginLeft:20}}>
                        <Text style={{fontWeight:'bold'}}>{profile.first_name} {profile.last_name}</Text>
                        <Text>{profile.email}</Text>
                      </View>  
                  </View>
                </TouchableOpacity>
           </ListItem>
          )
       })
    }
    

    renderListMap(){
        return(
            // <Content>
                <MapView 
                       region={this.state.region}
                        style={styles.map} 
                        showsMyLocationButton={true}
                        showsUserLocation={true}
                        showsCompass={true}
                        showsTraffic={true}
                        ref={(ref) => { this.mapRef = ref }}
                        // onMapReady = {this.onLayout}        
                        >
                        {/* <Polyline coordinates={bandung_kota} strokeWidth={4} strokeColor="#2962FF" />  */}
                      <Marker
                        coordinate={{latitude : -6.92159288169113 , longitude : 107.61071122273493}}
                      >
                      </Marker>
                     
                      <Marker
                        coordinate={{latitude : -9.92159288169113 , longitude : 107.61071122273493}}
                      >
                      </Marker>

                    </MapView>
            // </Content>
        )
    }

    render(){

        return(
            <Container>
                
                {
                    this.state.maps==true ?
                    this.renderListMap():
                    <View style={styles.content}>
                        <List>
                            {this.renderListContent()}
                        </List>
                    </View>
                }
                
            </Container>
        )

    }
}

const styles = StyleSheet.create({

    container: {
        flex: 1,
    },
    image: {
        flex: 1,
        justifyContent: "center"
    },
    content: {
        flexGrow: 1,
        padding: 14,
        width: "100%",
      },
    headerImage: {
        alignItems : 'center',
        justifyContent : 'center',
        height : 182,
        backgroundColor : 'transparent',
        marginTop:windowHeight * 10/100
    },
    input: {
        fontSize: 14, 
        backgroundColor:'#FFF', 
        borderRadius:10,
        marginBottom : 13,
        height: 40
    },
    buttonContainer: {
        width: "100%",
        justifyContent: "center",
        alignItems: "center",
        alignSelf : 'center',
        marginTop: windowHeight * 35/100,
    },
    button: {
        backgroundColor : '#2B637B',
        width : '100%',
        justifyContent :'center',
        height:40,
        borderRadius: 10,
        marginBottom : 13,
    },
    image_profile: {
        width:50, 
        height:50, 
        resizeMode: 'contain',  
        borderRadius: 150 / 2,
        overflow: "hidden"
    },
    map: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
      },
})

User.propTypes = {
    errors: PropTypes.object.isRequired
  };
  
const mapStateToProps = state => ({
errors: state.errors,
user : state.users
});
  
export default connect(
    mapStateToProps,
    { setSingleUsername, getUserProfile, setSingleProfile }
  )(User);