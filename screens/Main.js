import React, {Component} from 'react';
import { Dimensions, StyleSheet, BackHandler, Platform, View, 
    Text, Image, TouchableOpacity, Alert, ToastAndroid, ScrollView, ImageBackground} from "react-native";
import {
    Button,
    Container,
    Card,
    CardItem,
    Body,
    Content,
    Header,
    Title,
    Left,
    Icon,
    Right, Item, Label, Input, Row, Col, Form
  } from "native-base";
import { StatusBar } from 'expo-status-bar';
import { render } from 'react-dom';
import { KeyboardAwareScrollView, KeyboardAwareFlatList } from 'react-native-keyboard-aware-scroll-view';
import { setSingleUsername } from '../actions/User';
import { connect } from "react-redux";
import PropTypes from "prop-types";

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

class Main extends Component{
    
    constructor(props){
        super(props);

        this.state = {
            name : "",
            palindrome : ""
        }

    }

    palindromCheck = () => {

        let palindrome = this.state.palindrome.toLocaleLowerCase();
        if(palindrome == palindrome.split('').reverse().join('')){
            alert("isPalindrome")
        }else{
            alert("not palindrome")
        }

    }

    nextButton =()=>{

        this.props.setSingleUsername(this.state.name)
        this.props.navigation.navigate('Home')
    }

    render(){

        return(
            <Container>
                
                <ImageBackground source={require('../assets/background_login.png')} resizeMode='cover' style={styles.image}>
                    <KeyboardAwareScrollView
                        enableOnAndroid
                        enableAutomaticScroll
                        keyboardOpeningTime={0}
                        extraHeight={Platform.select({ android: 200 })}
                        style={{width:windowWidth}}
                        keyboardShouldPersistTaps="handled"
                        >
                        <View style={styles.content}>
                            <View style={styles.headerImage}>
                                <Image
                                    source={require('../assets/btn_add_photo.png')} style={{width:100, height:200, alignSelf:'center', resizeMode: 'contain'}}
                                />
                            </View>
                            <View style={styles.content}>
                                <Item floatingLabel style={{borderBottomColor:'rgba(158, 150, 150, 0)'}}>
                                    <Input 
                                        onChangeText={name => this.setState({ name })}
                                        value={this.state.name}
                                        placeholder={'Name'}
                                        placeholderTextColor={'#919190'}
                                        style={styles.input}
                                    />
                                </Item>
                                <Item floatingLabel style={{borderBottomColor:'rgba(158, 150, 150, 0)'}}>
                                    <Input 
                                        onChangeText={palindrome => this.setState({ palindrome })}
                                        value={this.state.palindrome}
                                        autoCapitalize={'none'}
                                        placeholder={'Palindrome'}
                                        placeholderTextColor={'#919190'}
                                        style={styles.input}
                                    />
                                </Item>
                                <View style={styles.buttonContainer}>
                                    <Button style={styles.button} onPress={()=>this.palindromCheck()}>
                                        <Text style={{color:'#fff', fontSize:14}}>CHECK</Text>
                                    </Button>
                                    <Button style={styles.button} onPress={()=>this.nextButton()}>
                                        <Text style={{color:'#fff', fontSize:14}}>NEXT</Text>
                                    </Button>
                                </View>
                            </View>
                        </View>
                    </KeyboardAwareScrollView>
                </ImageBackground>
                
            </Container>
        )

    }
}

const styles = StyleSheet.create({

    container: {
        flex: 1,
    },
    image: {
        flex: 1,
        justifyContent: "center"
    },
    content: {
        flexGrow: 1,
        padding: 14,
        width: "100%",
      },
    headerImage: {
        alignItems : 'center',
        justifyContent : 'center',
        height : 182,
        backgroundColor : 'transparent',
        marginTop:windowHeight * 10/100
    },
    input: {
        fontSize: 14, 
        backgroundColor:'#FFF', 
        borderRadius:10,
        marginBottom : 13,
        height: 40
    },
    buttonContainer: {
        width: "100%",
        justifyContent: "center",
        alignItems: "center",
        alignSelf : 'center',
        marginTop: 35,
    },
    button: {
        backgroundColor : '#2B637B',
        width : '100%',
        justifyContent :'center',
        height:40,
        borderRadius: 10,
        marginBottom : 13,
    },
})

Main.propTypes = {
    errors: PropTypes.object.isRequired
  };
  
const mapStateToProps = state => ({
errors: state.errors,
user : state.users
});
  
export default connect(
    mapStateToProps,
    { setSingleUsername }
  )(Main);