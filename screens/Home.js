import React, {Component} from 'react';
import { Dimensions, StyleSheet, BackHandler, Platform, View, 
    Text, Image, TouchableOpacity, Alert, ToastAndroid, ScrollView, ImageBackground} from "react-native";
import {
    Button,
    Container,
    Card,
    CardItem,
    Body,
    Content,
    Header,
    Title,
    Left,
    Icon,
    Right, Item, Label, Input, Row, Col, Form
  } from "native-base";
import { StatusBar } from 'expo-status-bar';
import { render } from 'react-dom';
import { KeyboardAwareScrollView, KeyboardAwareFlatList } from 'react-native-keyboard-aware-scroll-view';
import { setSingleUsername } from '../actions/User';
import { connect } from "react-redux";
import PropTypes from "prop-types";

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

class Home extends Component{
    
    constructor(props){
        super(props);

        this.state = {
            name : "",
            palindrome : ""
        }

    }

    palindromCheck = () => {

        let palindrome = this.state.palindrome.toLocaleLowerCase();
        if(palindrome == palindrome.split('').reverse().join('')){
            alert("isPalindrome")
        }else{
            alert("not palindrome")
        }

    }

    nextButton =()=>{

        this.props.setSingleUsername(this.state.name)
        
    }

    static navigationOptions = ({ navigation }) => ({

        headerLeft: () => 
        <Icon
        onPress={() => alert('This is a button!')}
        name="arrow-back"
        color="#2B637B"
        />
    })

    onPressWebsite=()=>{
        alert("ASDsa")
    }
    

    render(){

        console.log("ini", this.props.user.profile)

        return(
            <Container>
                
                <View style={styles.content}>
                    <View>
                        <Text style={{fontSize:16}}>Welcome</Text>
                        <Text style={{fontSize:16, fontWeight:'bold'}}>{this.props.user.user}</Text>
                    </View>
                    {
                        (this.props.user.profile==undefined || Object.entries(this.props.user.profile).length === 0)?
                        (
                            <View style={styles.headerImage}>
                                <Image
                                    source={require('../assets/user-select.png')} style={{width:150, height:150, alignSelf:'center', resizeMode: 'contain'}}
                                />
                                <View style={{marginTop:20}}>
                                    <Text style={{fontSize:16}}>Select a user to show the profile</Text>
                                </View>
                            </View>
                        ):
                        (
                            <View>
                            <View style={styles.headerImage}>
                                <Image
                                    source={{uri: this.props.user.profile.avatar}} style={{width:150, height:150, alignSelf:'center', resizeMode: 'contain'}}
                                />
                                <View style={{marginTop:20}}>
                                    <Text style={{fontSize:18, fontWeight:'600', color:'#626166'}}>{this.props.user.profile.first_name} {this.props.user.profile.last_name}</Text>
                                </View>
                                <View style={{marginTop:10}}>
                                    <Text style={{fontSize:16, color:'#626166'}}>{this.props.user.profile.email}</Text>
                                </View>
                                
                                
                            </View>
                            <View style={{marginTop:20, alignSelf:'center'}}>
                                <Text style={{fontSize:16, color:'#2B637B'}} onPress={()=>this.props.navigation.navigate('WebSite')}>Website</Text>
                            </View>
                            </View>
                        )
                    }
                    
                    <View style={styles.buttonContainer}>
                        <Button style={styles.button} onPress={()=>this.props.navigation.navigate('User')}>
                            <Text style={{color:'#fff', fontSize:14}}>Choose a user</Text>
                        </Button>
                    </View>
                </View>
                
            </Container>
        )

    }
}

const styles = StyleSheet.create({

    container: {
        flex: 1,
    },
    image: {
        flex: 1,
        justifyContent: "center"
    },
    content: {
        flexGrow: 1,
        padding: 14,
        width: "100%",
      },
    headerImage: {
        alignItems : 'center',
        justifyContent : 'center',
        height : 182,
        backgroundColor : 'transparent',
        marginTop:windowHeight * 10/100
    },
    input: {
        fontSize: 14, 
        backgroundColor:'#FFF', 
        borderRadius:10,
        marginBottom : 13,
        height: 40
    },
    buttonContainer: {
        width: "100%",
        justifyContent: "center",
        alignItems: "center",
        alignSelf : 'center',
        marginTop: windowHeight * 30/100,
    },
    button: {
        backgroundColor : '#2B637B',
        width : '100%',
        justifyContent :'center',
        height:40,
        borderRadius: 10,
        marginBottom : 13,
    },
})

Home.propTypes = {
    errors: PropTypes.object.isRequired
  };
  
const mapStateToProps = state => ({
errors: state.errors,
user : state.users
});
  
export default connect(
    mapStateToProps,
    { setSingleUsername }
  )(Home);