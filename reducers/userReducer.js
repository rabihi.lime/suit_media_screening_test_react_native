// authReducer.js

import { SET_USER_NAME, SET_USER_PROFILE, SET_USER_PROFILE_ONE} from '../actions/types';

const initialState = {
    user : {},
    user_profile : [],
    profile : {}
}

export default function(state = initialState, action ) {
    switch(action.type) {
        case SET_USER_NAME:
            return {
                ...state,
                user : action.payload,
            }
        case SET_USER_PROFILE:
            return {
                ...state,
                user_profile : action.payload,
            }
        case SET_USER_PROFILE_ONE:
            return {
                ...state,
                profile : action.payload,
            }
        default: 
            return state;
    }
}