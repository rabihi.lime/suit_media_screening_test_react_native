// types.js

// import { REACT_NATIVE_APP_PROD_URL_REST, REACT_NATIVE_APP_DEV_URL_REST } from 'react-native-dotenv'

// export const URL_REST = process.env.NODE_ENV === 'production' ?  REACT_NATIVE_APP_PROD_URL_REST : REACT_NATIVE_APP_DEV_URL_REST  ;

export const GET_ERRORS = 'GET_ERRORS';
export const SET_USER_NAME = 'SET_USER_NAME'
export const SET_USER_PROFILE = 'SET_USER_PROFILE'

export const SET_USER_PROFILE_ONE = 'SET_USER_PROFILE_ONE'