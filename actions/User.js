// Mail.js

import { SET_USER_NAME, SET_USER_PROFILE, SET_USER_PROFILE_ONE } from "./types";
import axios from "axios";


export const setSingleUsername = (user) => dispatch => {
    dispatch(setUsername(user));
    console.log(user)
  };

  export const setUsername = user => {
    return {
      type: SET_USER_NAME,
      payload: user
    };
  }

  export const getUserProfile = (users) => dispatch => {
    axios({
      method: "get",
      url: "https://reqres.in/api/users",
    })
    .then(res => {
      dispatch(serUserProfile(res.data.data));
      
    }).catch(err => {
      console.log(err)
    });
  };

  export const serUserProfile = users => {
    return {
      type: SET_USER_PROFILE,
      payload: users
    };
  }

  export const setSingleProfile = (profile) => dispatch => {
    dispatch(setProfileOne(profile));
    console.log(profile)
  };

  export const setProfileOne = profile => {
    return {
      type: SET_USER_PROFILE_ONE,
      payload: profile
    };
  }