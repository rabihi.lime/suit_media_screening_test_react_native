import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import {NativeBaseProvider} from 'native-base'
import Navigator from './navigators/Navigator'
import { Provider } from 'react-redux';
import store from './store';


export default function App() {
  return (
    <Provider store={store}>
        <View style={styles.container}>
          <Navigator />
        </View>
    </Provider>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
});
